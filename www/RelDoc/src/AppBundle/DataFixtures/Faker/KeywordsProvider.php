<?php

namespace AppBundle\DataFixtures\Faker;
use Faker\Provider\Lorem;
use Faker\Provider\Base;

/**
 * @author Dominik Firla
 */
class KeywordsProvider {
	/**
	 * Returns array of 1 to 5 random words to be used as keyword fixture for Art.
	 *
	 * @return array
	 */
	public static function keywords() {
		$numberOfKeywords = Base::numberBetween(1, 5);
		return Lorem::words($numberOfKeywords);
	}
}
