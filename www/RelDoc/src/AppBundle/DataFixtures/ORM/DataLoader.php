<?php

namespace AppBundle\DataFixtures\ORM;

use Hautelook\AliceBundle\Doctrine\DataFixtures\AbstractLoader;

/**
 * @author Dominik Firla
 */
class DataLoader extends AbstractLoader {

	/**
	 * {@inheritdoc}
	 */
	public function getFixtures() {
		return [
			'@AppBundle/DataFixtures/ORM/LoadArts.yml',
//			'@AppBundle/DataFixtures/ORM/LoadArts_sample.yml',
		];
	}
}
