<?php

namespace AppBundle\DataFixtures\ODM;

use Hautelook\AliceBundle\Doctrine\DataFixtures\AbstractLoader;

/**
 * @author Dominik Firla
 */
class DataLoader extends AbstractLoader {

	/**
	 * {@inheritdoc}
	 */
	public function getFixtures() {
		return [
			'@AppBundle/DataFixtures/ODM/LoadArts_part1.yml',
			'@AppBundle/DataFixtures/ODM/LoadArts_part2.yml',
			'@AppBundle/DataFixtures/ODM/LoadArts_part3.yml',
			'@AppBundle/DataFixtures/ODM/LoadArts_part4.yml',
//			'@AppBundle/DataFixtures/ORM/LoadArts_sample.yml',
		];
	}
}
