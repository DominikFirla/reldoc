<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 * @author Dominik Firla
 * @ORM\Table(name="Author")
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Author {
	/**
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(name="first_name", type="string", length=50, nullable=false)
	 */
	private $firstName;

	/**
	 * @var string
	 * @ORM\Column(name="last_name", type="string", length=50, nullable=false)
	 */
	private $lastName;

	/**
	 * @var ArrayCollection|ArtTag[]
	 *
	 * @ORM\OneToMany(targetEntity="Art", mappedBy="author", orphanRemoval=true, cascade={"persist"})
	 */
	private $arts;

	public function __construct() {
		$this->arts = new ArrayCollection();
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	/**
	 * @param Art $art
	 */
	public function addArt(Art $art) {
		$this->arts[] = $art;
		$art->setAuthor($this);
	}

	/**
	 * @param Art $art
	 */
	public function removeArt(Art $art) {
		$this->arts->removeElement($art);
	}

	/**
	 * @return ArrayCollection|Art[]
	 */
	public function getArts() {
		return $this->arts;
	}
}
