<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArtReview
 * @author Dominik Firla
 *
 * @ORM\Table(name="ArtReview")
 * @ORM\Entity()
 */
class ArtReview {
	/**
	 * @var int
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Art
	 *
	 * @ORM\ManyToOne(targetEntity="Art", inversedBy="reviews")
	 * @ORM\JoinColumn(name="art_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $art;

	/**
	 * @var Reviewer
	 *
	 * @ORM\ManyToOne(targetEntity="Reviewer", inversedBy="arts")
	 * @ORM\JoinColumn(name="reviewer_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $reviewer;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="text", type="text", nullable=false)
	 */
	private $text;

	/**
	 * @return null|int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return Art
	 */
	public function getArt() {
		return $this->art;
	}

	/**
	 * @param Art $art
	 */
	public function setArt(Art $art) {
		$this->art = $art;
	}

	/**
	 * @return Review
	 */
	public function getReviewer() {
		return $this->reviewer;
	}

	/**
	 * @param Reviewer $reviewer
	 */
	public function setReviewer(Reviewer $reviewer) {
		$this->reviewer = $reviewer;
	}

	/**
	 * @return string
	 */
	public function getText() {
		return $this->text;
	}

	/**
	 * @param string $text
	 */
	public function setText($text) {
		$this->text = $text;
	}
}