<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Keyword
 * @author Dominik Firla
 * @ORM\Table(name="Keyword")
 *
 * @ORM\Entity()
 */
class Keyword {
	/**
	 * @var int
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Art
	 *
	 * @ORM\ManyToOne(targetEntity="Art", inversedBy="keywords")
	 * @ORM\JoinColumn(name="art_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $art;

	/**
	 * @var string
	 * @ORM\Column(name="keyword", type="string", length=255, nullable=false)
	 */
	private $keyword;

	/**
	 * @return null|int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return Art
	 */
	public function getArt() {
		return $this->art;
	}

	/**
	 * @param Art $art
	 */
	public function setArt(Art $art) {
		$this->art = $art;
	}

	/**
	 * @return
	 */
	public function getKeyword() {
		return $this->keyword;
	}

	/**
	 * @param $keyword
	 */
	public function setKeyword($keyword) {
		$this->keyword = $keyword;
	}
}