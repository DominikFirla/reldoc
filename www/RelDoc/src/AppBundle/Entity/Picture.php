<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Picture
 * @author Dominik Firla
 * @ORM\Entity()
 */
class Picture extends Art {
	/**
	 * @ORM\Column(name="resolution_x", type="integer", nullable=false)
	 */
	private $resolutionX;

	/**
	 * @ORM\Column(name="resolution_y", type="integer", nullable=false)
	 */
	private $resolutionY;

	/**
	 * @return int
	 */
	public function getResolutionX() {
		return $this->resolutionX;
	}

	/**
	 * @param int $resolutionX
	 */
	public function setResolutionX($resolutionX) {
		$this->resolutionX = $resolutionX;
	}

	/**
	 * @return int
	 */
	public function getResolutionY() {
		return $this->resolutionY;
	}

	/**
	 * @param int $resolutionY
	 */
	public function setResolutionY($resolutionY) {
		$this->resolutionY = $resolutionY;
	}
}