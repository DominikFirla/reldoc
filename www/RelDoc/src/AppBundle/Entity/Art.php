<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Art
 * @author Dominik Firla
 * @ORM\Table(name="Art")
 *
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"music" = "Music", "picture" = "Picture"})
 */
abstract class Art {
	/**
	 * @var integer
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var Author
	 *
	 * @ORM\ManyToOne(targetEntity="Author", inversedBy="arts")
	 * @ORM\JoinColumn(name="author_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $author;

	/**
	 * @var string
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @var float
	 * @ORM\Column(name="price", type="float", nullable=false)
	 */
	private $price = 0;

	/**
	 * @var int
	 * @ORM\Column(name="commercial_price", type="float", nullable=true)
	 */
	private $commercialPrice;

	/**
	 * @var bool
	 * @ORM\Column(name="commercial", type="boolean", nullable=false)
	 */
	private $commercial = false;

	/**
	 * @var int
	 * @ORM\Column(name="size", type="integer", nullable=false)
	 */
	private $size;

	/**
	 * @var float
	 * @ORM\Column(name="avg_rating", type="float", nullable=false)
	 */
	private $avgRating = 0;

	/**
	 * @var int
	 * @ORM\Column(name="rating_count", type="integer", nullable=false)
	 */
	private $ratingCount = 0;

	/**
	 * @var \DateTime
	 * @ORM\Column(name="date_published", type="date", nullable=false)
	 */
	private $datePublished;

	/**
	 * @var ArrayCollection|Keyword[]
	 *
	 * @ORM\OneToMany(targetEntity="Keyword", mappedBy="art", orphanRemoval=true, cascade={"persist"})
	 *
	 */
	private $keywords;

	/**
	 * @var ArrayCollection|ArtReview[]
	 *
	 * @ORM\OneToMany(targetEntity="ArtReview", mappedBy="art", orphanRemoval=true, cascade={"persist"})
	 *
	 */
	private $reviews;

	public function __construct() {
		$this->keywords = new ArrayCollection();
		$this->reviews = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	/**
	 * @return Author
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * @param Author $author
	 */
	public function setAuthor(Author $author) {
		$this->author = $author;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return float
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice($price) {
		$this->price = $price;
	}

	/**
	 * @return float
	 */
	public function getCommercialPrice() {
		return $this->commercialPrice;
	}

	/**
	 * @param float $commercialPrice
	 */
	public function setCommercialPrice($commercialPrice) {
		$this->commercialPrice = $commercialPrice;
	}

	/**
	 * @return float
	 */
	public function isCommercial() {
		return $this->commercial;
	}

	/**
	 * @param float $commercial
	 */
	public function setCommercial($commercial) {
		$this->commercial = $commercial;
	}

	/**
	 * @return int
	 */
	public function getSize() {
		return $this->size;
	}

	/**
	 * @param int $size
	 */
	public function setSize($size) {
		$this->size = $size;
	}

	/**
	 * @return float
	 */
	public function getAvgRating() {
		return $this->avgRating;
	}

	/**
	 * @param float $avgRating
	 */
	public function setAvgRating($avgRating) {
		$this->avgRating = $avgRating;
	}

	/**
	 * @return int
	 */
	public function getRatingCount() {
		return $this->ratingCount;
	}

	/**
	 * @param int $ratingCount
	 */
	public function setRatingCount($ratingCount) {
		$this->ratingCount = $ratingCount;
	}

	/**
	 * @return \DateTime
	 */
	public function getDatePublished() {
		return $this->datePublished;
	}

	/**
	 * @param \DateTime $datePublished
	 */
	public function setDatePublished(\DateTime $datePublished) {
		$this->datePublished = $datePublished;
	}

	/**
	 * @param Keyword $keyword
	 */
	public function addKeyword(Keyword $keyword) {
		$this->keywords[] = $keyword;
		$keyword->setArt($this);
	}

	/**
	 * @param Keyword $keyword
	 */
	public function removeKeyword(Keyword $keyword) {
		$this->keywords->removeElement($keyword);
	}

	/**
	 * @return ArrayCollection|Keyword[]
	 */
	public function getKeywords() {
		return $this->keywords;
	}

	/**
	 * @param ArtReview $artReview
	 */
	public function addReview(ArtReview $artReview) {
		$this->reviews[] = $artReview;
		$artReview->setArt($this);
	}

	/**
	 * @param ArtReview $artReview
	 */
	public function removeReview(ArtReview $artReview) {
		$this->reviews->removeElement($artReview);
	}

	/**
	 * @return ArrayCollection|ArtReview[]
	 */
	public function getReviews() {
		return $this->reviews;
	}

	/**
	 * @ORM\PrePersist()
	 * @return void
	 */
	public function prePersist() {
		$this->setDatePublished(new \DateTime());
	}
}
