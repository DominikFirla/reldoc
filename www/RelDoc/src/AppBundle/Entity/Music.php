<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Music
 * @author Dominik Firla
 * @ORM\Entity()
 */
class Music extends Art {
	/**
	 * @ORM\Column(name="length", type="integer", nullable=false)
	 * @Assert\NotNull()
	 * @Assert\Type(type="integer")
	 */
	private $length;

	/**
	 * @return int
	 */
	public function getLength() {
		return $this->length;
	}

	/**
	 * @param int $length
	 */
	public function setLength($length) {
		$this->length = $length;
	}
}