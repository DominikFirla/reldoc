<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @author Dominik Firla
 * @MongoDB\Document
 * @MongoDB\MappedSuperclass
 * @MongoDB\HasLifecycleCallbacks
 * @MongoDB\InheritanceType("SINGLE_COLLECTION")
 * @MongoDB\DiscriminatorField("type")
 * @MongoDB\DiscriminatorMap({"music"="Music", "picture"="Picture"})
 */
abstract class Art {

	/**
	 * @MongoDB\Id
	 */
	private $id;

	/**
	 * @MongoDB\ReferenceOne(targetDocument="Author", cascade={"all"})
	 */
	private $author;

	/**
	 * @MongoDB\String
	 */
	private $name;

	/**
	 * @MongoDB\String
	 */
	private $description;

	/**
	 * @MongoDB\Float
	 */
	private $price;

	/**
	 * @MongoDB\Float
	 */
	private $commercialPrice;

	/**
	 * @MongoDB\Boolean
	 */
	private $commercial;

	/**
	 * @MongoDB\Int
	 */
	private $size;

	/**
	 * @MongoDB\Float
	 */
	private $avgRating;

	/**
	 * @MongoDB\Int
	 */
	private $ratingCount;

	/**
	 * @MongoDB\Date
	 */
	private $datePublished;

	/**
	 * @MongoDB\Collection
	 */
	private $keywords = array();

	/**
	 * @MongoDB\EmbedMany(targetDocument="ArtReview")
	 */
	private $reviews;

	public function __construct() {
		$this->reviews = new ArrayCollection();
	}

	public function getId() {
		return $this->id;
	}

	/**
	 * @return Author
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * Modified for fixture data generation. Use override=true to change once set author.
	 *
	 * @param Author $author
	 * @param bool $override
	 */
	public function setAuthor(Author $author, $override = false) {
		if (!$override) {
			if (!$this->author) {
				$this->author = $author;
				$author->addArt($this);
			}
		} else {
			$this->author = $author;
		}
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return float
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice($price) {
		$this->price = $price;
	}

	/**
	 * @return float
	 */
	public function getCommercialPrice() {
		return $this->commercialPrice;
	}

	/**
	 * @param float $commercialPrice
	 */
	public function setCommercialPrice($commercialPrice) {
		$this->commercialPrice = $commercialPrice;
	}

	/**
	 * @return bool
	 */
	public function getCommercial() {
		return $this->commercial;
	}

	/**
	 * @param bool $commercial
	 */
	public function setCommercial($commercial) {
		$this->commercial = $commercial;
	}

	/**
	 * @return int
	 */
	public function getSize() {
		return $this->size;
	}

	/**
	 * @param int $size
	 */
	public function setSize($size) {
		$this->size = $size;
	}

	/**
	 * @return float
	 */
	public function getAvgRating() {
		return $this->avgRating;
	}

	/**
	 * @param float $avgRating
	 */
	public function setAvgRating($avgRating) {
		$this->avgRating = $avgRating;
	}

	/**
	 * @return int
	 */
	public function getRatingCount() {
		return $this->ratingCount;
	}

	/**
	 * @param int $ratingCount
	 */
	public function setRatingCount($ratingCount) {
		$this->ratingCount = $ratingCount;
	}

	/**
	 * @return \DateTime
	 */
	public function getDatePublished() {
		return $this->datePublished;
	}

	/**
	 * @param \DateTime $datePublished
	 */
	public function setDatePublished(\DateTime $datePublished) {
		$this->datePublished = $datePublished;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getReviews() {
		return $this->reviews;
	}

	/**
	 * Add review
	 *
	 * @param AppBundle\Document\ArtReview $review
	 */
	public function addReview(\AppBundle\Document\ArtReview $review) {
		$this->reviews[] = $review;
	}

	/**
	 * Remove review
	 *
	 * @param AppBundle\Document\ArtReview $review
	 */
	public function removeReview(\AppBundle\Document\ArtReview $review) {
		$this->reviews->removeElement($review);
	}

    /**
	 * Set keywords
	 *
	 * @param collection $keywords
	 */
	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}

	/**
	 * Get keywords
	 *
	 * @return collection $keywords
	 */
	public function getKeywords() {
		return $this->keywords;
	}

	/**
	 * @MongoDB\PrePersist
	 */
	public function prePersist() {
		$this->setDatePublished(new \DateTime());
	}
}
