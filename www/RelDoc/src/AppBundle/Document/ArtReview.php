<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * ArtReview
 * @author Dominik Firla
 * @MongoDB\EmbeddedDocument
 */
class ArtReview {

	/**
	 * @MongoDB\Id
	 */
	private $id;

	/**
	 * For fixture purpose only.
	 */
	private $art;

	/**
	 * @MongoDB\ReferenceOne(targetDocument="Reviewer")
	 */
	private $reviewer;

	/**
	 * @MongoDB\String
	 */
	private $text;

	/**
	 * @return null|int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return Art
	 */
	public function getArt() {
		return $this->art;
	}

	/**
	 * Modified for fixture data generation. Use override=true to change once set author.
	 *
	 * @param Art $art
	 * @param bool $override
	 */
	public function setArt(Art $art, $override = false) {
		if (!$override) {
			if (!$this->art) {
				$this->art = $art;
				$art->addReview($this);
			}
		} else {
			$this->art = $art;
		}
	}

	/**
	 * @return Review
	 */
	public function getReviewer() {
		return $this->reviewer;
	}

	/**
	 * @param Reviewer $reviewer
	 */
	public function setReviewer(Reviewer $reviewer) {
		$this->reviewer = $reviewer;
	}

	/**
	 * @return string
	 */
	public function getText() {
		return $this->text;
	}

	/**
	 * @param string $text
	 */
	public function setText($text) {
		$this->text = $text;
	}
}
