<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @author Dominik Firla
 * @MongoDB\Document
 */
class Music extends Art {
	/**
     * @MongoDB\Int
     */
	private $length;

	/**
	 * @return int
	 */
	public function getLength() {
		return $this->length;
	}

	/**
	 * @param int $length
	 */
	public function setLength($length) {
		$this->length = $length;
	}
}
