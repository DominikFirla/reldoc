<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @author Dominik Firla
 * @MongoDB\Document
 */
class Author {

	/**
	 * @MongoDB\Id
	 */
	private $id;

	/**
	 * @MongoDB\String
	 */
	private $firstName;

	/**
	 * @MongoDB\String
	 */
	private $lastName;

	/**
	 * @MongoDB\ReferenceMany(targetDocument="Art", cascade={"persist"})
	 */
	private $arts;

	public function __construct() {
		$this->arts = new ArrayCollection();
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getFirstName() {
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 */
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	/**
	 * @return type
	 */
	public function getArts() {
		return $this->arts;
	}

	/**
	 * Add art
	 *
	 * @param Art $art
	 */
	public function addArt(Art $art) {
		$this->arts[] = $art;
		$art->setAuthor($this);
	}

	/**
	 * Remove art
	 *
	 * @param AppBundle\Document\Art $art
	 */
	public function removeArt(\AppBundle\Document\Art $art) {
		$this->arts->removeElement($art);
	}
}
