<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @author Dominik Firla
 * @MongoDB\Document
 */
class Picture extends Art {
	/**
     * @MongoDB\Int
     */
	private $resolutionX;

	/**
     * @MongoDB\Int
     */
	private $resolutionY;

	/**
	 * @return int
	 */
	public function getResolutionX() {
		return $this->resolutionX;
	}

	/**
	 * @param int $resolutionX
	 */
	public function setResolutionX($resolutionX) {
		$this->resolutionX = $resolutionX;
	}

	/**
	 * @return int
	 */
	public function getResolutionY() {
		return $this->resolutionY;
	}

	/**
	 * @param int $resolutionY
	 */
	public function setResolutionY($resolutionY) {
		$this->resolutionY = $resolutionY;
	}
}
