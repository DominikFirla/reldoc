<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Author;
use AppBundle\Entity\Music;

/** @author Dominik Firla */
class MysqlController extends Controller {
    /**
     * @Route("/mysql/insert", name="mysql_insert")
     */
    public function insertAction(Request $request) {
		// creates object of Author and sets its mandatory properties
		$author = new Author();
		$author->setFirstName("Jan");
		$author->setLastName("Novák");

		// creates object of Music and sets its mandatory properties
		$music = new Music();
		$music->setName("Hudba 1");
		$music->setSize(150);
		$author->addArt($music); // adds Music to collection of Art on Author object

		$entityManager = $this->get("doctrine")->getManager(); // retrieves Doctrine\ORM\EntityManager
		$entityManager->persist($author); // schedules Author for insert
		$entityManager->flush(); // peforms all scheduled operations

		return $this->redirectToRoute('homepage');
	}

	/**
     * @Route("/mysql/select", name="mysql_select")
     */
    public function selectAction(Request $request) {
		$entityManager = $this->get("doctrine")->getManager();
		$result1 = $entityManager->getRepository('AppBundle:Author')->find(1); // find by ID: 1
		$result2 = $entityManager->getRepository('AppBundle:Author')->findOneByFirstName("Jan");
		$result3 = $entityManager->getRepository('AppBundle:Author')->findBy(array('lastName' => 'Novák'));
		\dump($result1);
//		exit;

		$queryBuilder = $entityManager->createQueryBuilder();
		$queryBuilder	->select('author')
						->from('AppBundle:Author', 'author')
						->leftJoin('AppBundle:Art', 'art', 'WITH', 'art.author = author')
						->leftJoin('AppBundle:Keyword', 'keyword', 'WITH', 'keyword.art = art')
						->where('art.commercial = true')
						->andWhere('keyword.keyword = :word')
						->setParameter('word', 'dolorem');
		$result4 = $queryBuilder->getQuery()->getResult();

		return $this->redirectToRoute('homepage');
	}

	/**
     * @Route("/mysql/update", name="mysql_update")
     */
    public function updateAction(Request $request) {
		$entityManager = $this->get("doctrine")->getManager();
		$author = $entityManager->getRepository('AppBundle:Author')->find(1);
		$author->setLastName('Nové přijmení');
		$entityManager->flush();

		return $this->redirectToRoute('homepage');
	}

	/**
     * @Route("/mysql/delete", name="mysql_delete")
     */
    public function deleteAction(Request $request) {
		$entityManager = $this->get("doctrine")->getManager();
		$author = $entityManager->getRepository('AppBundle:Author')->find(1);
		$entityManager->remove($author);
		$entityManager->flush();

		return $this->redirectToRoute('homepage');
	}
}
