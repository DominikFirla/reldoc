<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Dominik Firla
 */
class PerformanceController extends Controller {
	/**
	 * @Route("/performance", name="performance")
	 */
	public function performanceAction(Request $request) {
		// for most accurate performance monitoring, only one operation should
		// be performed on each request. Other lines should be commented.


//		$this->selectAuthorByArtKeyword();		// mysql
//		$this->selectAuthorByArtKeyword(true);	// mongo

//		$this->selectCommercialMusic();			// mysql
//		$this->selectCommercialMusic(true);		// mongo

		$this->updateMusicDatePublished();		// mysql
//		$this->updateMusicDatePublished(true);	// mongo

		return $this->redirectToRoute('homepage');
	}

	/**
	 * Returns all authors of Arts containing a keyword.
	 * MySQL database is queried by default, set parameter to true for MongoDB.
	 *
	 * @param bool $mongo
	 */
	private function selectAuthorByArtKeyword($mongo = false) {
		if (!$mongo) {
			$entityManager = $this->getDoctrine()->getManager();
			$queryBuilder = $entityManager->createQueryBuilder();
			$queryBuilder	->select('author')
						->from('AppBundle:Author', 'author')
						->leftJoin('AppBundle:Art', 'art', 'WITH', 'art.author = author')
						->leftJoin('AppBundle:Keyword', 'keyword', 'WITH', 'keyword.art = art')
						->where('art.commercial = true')
						->andWhere('keyword.keyword = :word')
						->setParameter('word', 'dolorem');
			$authors = $queryBuilder->getQuery()->getResult();
		} else {
			$documentManager = $this->get('doctrine_mongodb')->getManager();
			$artQueryBuilder = $documentManager->createQueryBuilder('AppBundle:Art');
			$artQueryBuilder->field('author')->prime(true)
							->field('commercial')->equals(true)
							->field('keywords')->in(array('dolorem'));
			$artsCursor = $artQueryBuilder->getQuery()->execute();
			$authors = [];
			foreach ($artsCursor as $art) {
				$authors[] = $art->getAuthor();
			}
		}
	}

	/**
	 * Returns commercial Music sorted by name with limit of 50.
	 * MySQL database is queried by default, set parameter to true for MongoDB.
	 * @param bool $mongo
	 */
	private function selectCommercialMusic($mongo = false) {
		if (!$mongo) {
			$entityManager = $this->getDoctrine()->getManager();
			$musics = $entityManager->getRepository('AppBundle:Music')->findBy(
					array('commercial' => true),
					array('name' => 'ASC'),
					50
			);
		} else {
			$documentManager = $this->get('doctrine_mongodb')->getManager();
			$musics = $documentManager->getRepository('AppBundle:Music')->findBy(
					array('commercial' => false),
					array('name' => 'ASC'),
					50
			);
		}
	}

	/**
	 * Update datePublished field for all Music records to current date.
	 * MySQL database is queried by default, set parameter to true for MongoDB.
	 * @param bool $mongo
	 */
	private function updateMusicDatePublished($mongo = false) {
		if (!$mongo) {
			$entityManager = $this->getDoctrine()->getManager();
			$queryBuilder = $entityManager->createQueryBuilder();
			$updateQuery = $queryBuilder
					->update('AppBundle:Music', 'music')
					->set('music.datePublished', ':now')
					->setParameter('now', new \DateTime())
					->getQuery();
			$updateQuery->execute();
		} else {
			$documentManager = $this->get('doctrine_mongodb')->getManager();
			$documentManager->createQueryBuilder('AppBundle:Music')
					->update()
					->multiple(true)
					->field('datePublished')->set(new \MongoDate())
					->getQuery()
					->execute();
		}
	}
}
