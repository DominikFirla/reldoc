<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Dominik Firla
 */
class DefaultController extends Controller {
	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction(Request $request) {

		return $this->render('AppBundle:Default:homepage.html.twig', array(
					'mysql' => array(
						'insert' => $this->generateUrl('mysql_insert'),
						'select' => $this->generateUrl('mysql_select'),
						'update' => $this->generateUrl('mysql_update'),
						'delete' => $this->generateUrl('mysql_delete'),
					),
					'mongo' => array(
						'insert' => $this->generateUrl('mongo_insert'),
						'select' => $this->generateUrl('mongo_select'),
						'update' => $this->generateUrl('mongo_update'),
						'delete' => $this->generateUrl('mongo_delete'),
					),
		));
	}
}
