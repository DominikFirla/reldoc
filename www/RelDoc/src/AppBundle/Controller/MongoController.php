<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Document\Author;
use AppBundle\Document\Music;

/**
 * @author Dominik Firla
 */
class MongoController extends Controller {
    /**
     * @Route("/mongo/insert", name="mongo_insert")
     */
    public function insertAction(Request $request) {
		// creates object of Author and sets its mandatory properties
		$author = new Author();
		$author->setFirstName("Jan");
		$author->setLastName("Novák");

		// creates object of Music and sets its mandatory properties
		$music = new Music();
		$music->setName("Hudba 1");
		$music->setSize(150);
		
		$author->addArt($music); // adds Music to collection of Art on Author object

		$documentManager = $this->get('doctrine_mongodb')->getManager(); // retrieves Doctrine\ODM\MongoDB\DocumentManager
		$documentManager->persist($author); // schedules Author for insert
		$documentManager->flush(); // peforms all scheduled operations

		return $this->redirectToRoute('homepage');
	}

	/**
     * @Route("/mongo/select", name="mongo_select")
     */
    public function selectAction(Request $request) {
		$documentManager = $this->get('doctrine_mongodb')->getManager();

		$result1 = $documentManager->getRepository('AppBundle:Author')->findOneByFirstName("Jan");
		$result2 = $documentManager->getRepository('AppBundle:Author')->findBy(array('lastName' => 'Novák'));
		$result3 = $documentManager->getRepository('AppBundle:Author')->find($result1->getId());

		$artQueryBuilder = $documentManager->createQueryBuilder('AppBundle:Art');
		$artQueryBuilder->field('commercial')->equals(true)
						->field('keywords')->in(array('dolorem'));
		$result4 = $artQueryBuilder->getQuery()->execute();
		foreach ($result4 as $art) {
			$art->getAuthor();
		}

		return $this->redirectToRoute('homepage');
	}

	/**
     * @Route("/mongo/update", name="mongo_update")
     */
    public function updateAction(Request $request) {
		$documentManager = $this->get('doctrine_mongodb')->getManager();
		$author = $documentManager->getRepository('AppBundle:Author')->findOneByFirstName("Jan");
		$author->setLastName('Nové přijmení');
		$documentManager->flush();

		return $this->redirectToRoute('homepage');
	}

	/**
     * @Route("/mongo/delete", name="mongo_delete")
     */
    public function deleteAction(Request $request) {
		$documentManager = $this->get('doctrine_mongodb')->getManager();
		$author = $documentManager->getRepository('AppBundle:Author')->findOneByFirstName("Jan");
		$documentManager->remove($author);
		$documentManager->flush();

		return $this->redirectToRoute('homepage');
	}
}
